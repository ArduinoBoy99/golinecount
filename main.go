// main
package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

func main() {
	totalCount := 0
	suffix := flag.String("suffix", ".go", "What suffix to search for")
	flag.Parse()

	files, err := walkTheWalk(*suffix)
	if err != nil {
		fmt.Println("Error on walking the directory tree. Error:", err)
	}
	var wg sync.WaitGroup
	var mutex = &sync.Mutex{}

	for _, file := range files {
		wg.Add(1)
		byteFile, err := os.Open(file)
		if err != nil {
			fmt.Println("Error on opening file. Error:", err)
			break
		}

		go func() {
			defer wg.Done()

			count, err := lineCounter(byteFile)
			if err != nil {
				fmt.Println("Error on counting lines. Error:", err)
				return
			}
			mutex.Lock()
			totalCount = totalCount + count
			mutex.Unlock()
		}()
	}

	wg.Wait()
	fmt.Println("Number of lines:", totalCount)
}

func lineCounter(r io.Reader) (int, error) {
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		if err != nil {
			if err == io.EOF {
				return count, nil
			}
			return count, err
		}
	}
}

func walkTheWalk(suffix string) (files []string, err error) {
	files = []string{}
	err = filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if strings.HasSuffix(path, suffix) {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		log.Println(err)
		return files, err
	}
	return files, nil
}
