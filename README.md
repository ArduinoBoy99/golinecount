# GoLineCount

Go program searches recursively directories for files
with given extension and counts the total number of lines.

Usefull for counting number of lines of code in a project.

Usage example:

./LineCounter --sufix .go